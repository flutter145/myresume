import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Widget titleSection1 = Container(
      padding: const EdgeInsets.all(10),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Text(
                    'ประวัติส่วนตัว',
                    style: TextStyle(fontSize: 70,fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
    Widget textSection1 = Container(
      padding: const EdgeInsets.all(10),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Text(
                    'ชื่อ : สิรวิชญ์'+'\n'+'นามสกุล : เหรียญเจริญ'+'\n'+'อายุ : 23 ปี'+'\n'+'สัญชาติ : ไทย ศาลสนา : พุทธ'
                    +'\n'+'วันเกิด : 26 สิงหาคม 2542'+'\n'+'กำลังศึกษา : มหาวิทยาลัยบูรพา'+'\n'+'คณะ : วิทยาการสารสนเทศ'
                    +'\n'+'สาขา : วิทยาการคอมพิวเตอร์'+'\n'+'GPA : 2.45'+'\n'+'E-Mail : 61160168@go.buu.ac.th',
                    style: TextStyle(fontSize: 40),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
    Widget titleSection2 = Container(
      padding: const EdgeInsets.all(50),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Text(
                    'My Skill',
                    style: TextStyle(fontSize: 70,fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Image.asset('images/html.png',width: 200,height: 200,),
                      Image.asset('images/css.png',width: 200,height: 200,),
                      Image.asset('images/js.png',width: 200,height: 200,),
                      Image.asset('images/python.png',width: 200,height: 200,),
                      Image.asset('images/scratch.png',width: 200,height: 200,),
                      Image.asset('images/java.png',width: 200,height: 200,)
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
    Widget titleSection3 = Container(
      padding: const EdgeInsets.all(10),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Text(
                    'My Eduction',
                    style: TextStyle(fontSize: 70,fontWeight: FontWeight.bold),
                  ),
                ),
                Text('1.โรงเรียนอนุบาลขวัญตา(อนุบาลศึกษาปีที่1-6)'+
                '\n'+'2.โรงเรียนปรีชานุศาจน์(ประถมศึกษาปีที่1-6)'+
                '\n'+'3.โรงเรียนชลบุรีสุขบท(มัธยมศึกษาปีที่1-6)'+
                '\n'+'4.มหาวิทยาลัยบูรพา(ปริญญาตรี)',
                style: TextStyle(fontSize: 40),)
              ],
            ),
          )
        ],
      ),
    );
    return MaterialApp(
      title: 'My Resume',
      theme: ThemeData(primaryColor: Colors.black),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.perm_identity,size: 50,),
          title: const Text('My Resume',style: TextStyle(fontSize: 30),),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/srka.jpg',
              width: 500,
              height: 500,
            ),
            titleSection1,
            textSection1,
            titleSection2,
            titleSection3
          ],
        ),
      ),
    );
  }
}
